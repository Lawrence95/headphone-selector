@extends('inc.admin')

@section('content')
<div class="container">
<a href="{{ route('headphone.index') }}" class='btn btn-primary'>Back</a>
<h1>create headphone</h1>
{!! Form::open(['action' => 'HeadphoneController@store', 'method' => 'POST']) !!}
        <div class="form-group">
            {{Form::label('Model', 'Model')}}
            {{Form::text('model','',['class' => 'form-control', 'placeholder' => 'model'])}}
        </div>
        <div class="form-group">
            {{Form::label('Brand', 'Brand')}}
            {{Form::select('company_id', $company, null, ['class' => 'form-control', 'placeholder' => '-Select manufacture Company-'])}}
        </div>
        <div class="form-group">
            {{Form::label('Driver', 'Driver')}}
            {{Form::text('driver','',['class' => 'form-control', 'placeholder' => 'XXmm driver type'])}}
        </div>
        <div class="form-group">
            {{Form::label('Lowest Frequency', 'Lowest Frequency')}}
            {{Form::text('lowest_frequency','',['class' => 'form-control', 'placeholder' => 'Lowest Frequency'])}}
        </div>
        <div class="form-group">
            {{Form::label('Highest Frequency', 'Highest Frequency')}}
            {{Form::text('highest_frequency','',['class' => 'form-control', 'placeholder' => 'Highest Frequency'])}}
        </div>
        <div class="form-group">
            {{Form::label('Genre', 'Genre')}}
            {{Form::select('genre', ['Music' => 'Music', 'Movie' => 'Movie', 'Gaming' =>'Gaming'], null, ['class' => 'form-control', 'placeholder' => '-Select Genre-'])}}
        </div>
        <div class="form-group">
            {{Form::label('Interface', 'Interface')}}
            {{Form::select('interface', ['Wired' => 'Wired', "Wireless" =>"Wireless"], null, ['class' => 'form-control', 'placeholder' => '-Select Interface-'])}}
        </div>
        {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}

</div>
@endsection