@extends('inc.admin')

@section('content')
<div class="container">
<a href="{{ route('headphone.index') }}" class='btn btn-primary'>Back</a>
<h1>Edit {{$headphone->model}}</h1>
{!! Form::open(['route' => ['headphone.update', $headphone->id]
, 'method' => 'PUT']) !!}
        <div class="form-group">
            {{Form::label('Model', 'Model')}}
            {{Form::text('model', $headphone->model,['class' => 'form-control', 'placeholder' => 'model'])}}
        </div>
        <div class="form-group">
            {{Form::label('Brand', 'Brand')}}
            {{Form::select('company_id', $company, $headphone->company_id, ['class' => 'form-control', 'placeholder' => '-Select manufacture Company-'])}}
        </div>
        <div class="form-group">
            {{Form::label('Driver', 'Driver')}}
            {{Form::text('driver',$headphone->driver,['class' => 'form-control', 'placeholder' => 'XXmm driver type'])}}
        </div>
        <div class="form-group">
            {{Form::label('Lowest Frequency', 'Lowest Frequency')}}
            {{Form::text('lowest_frequency',$headphone->lowest_frequency,['class' => 'form-control', 'placeholder' => 'Lowest Frequency'])}}
        </div>
        <div class="form-group">
            {{Form::label('Highest Frequency', 'Highest Frequency')}}
            {{Form::text('highest_frequency',$headphone->highest_frequency,['class' => 'form-control', 'placeholder' => 'Highest Frequency'])}}
        </div>
        <div class="form-group">
            {{Form::label('Genre', 'Genre')}}
            {{Form::select('genre', ['Music' => 'Music', 'Movie' => 'Movie', 'Gaming' =>'Gaming'], null, ['class' => 'form-control', 'placeholder' => '-Select Genre-'])}}
        </div>
        <div class="form-group">
            {{Form::label('Interface', 'Interface')}}
            {{Form::select('interface', ['Wired' => 'Wired', "Wireless" =>"Wireless"], null, ['class' => 'form-control', 'placeholder' => '-Select Interface-'])}}
        </div>
        {{Form::submit('Update', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}

</div>
@endsection