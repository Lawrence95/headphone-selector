<?php
$user = Auth::user();
?>
@extends('inc.admin')

@section('content')
<div class = "container">

<h1>Headphone List</h1>
@if($user->isA('admin'))
<a href="{{ route('headphone.create') }}" class='btn btn-primary'>New Headphone</a>
@endif
@if(count($headphones) > 0)
<table class="table">
    <thead>
        <tr>
            <th> ID</th>
            <th> Model</th>
            <th> Company  </th>
            <th> Activities </th>
        </tr>
    </thead>
    <tbody>
         @foreach($headphones as $i => $headphone)
          <tr>
            <td> {{$i+1}} </td>
            <td> {{$headphone->model}} </a></td>
            <td> {{$headphone->company->name}} </td>
            <td><a href="{{ route('headphone.show',['id'=>$headphone->id]) }}" class='btn btn-primary'> Details</a>
            @if($user->isA('admin'))
            <a href="{{ route('headphone.edit',['id'=>$headphone->id]) }}" class='btn btn-success'> Edit</a>
            <a href="{{ route('headphone.delete',['id'=>$headphone->id]) }}" class='btn btn-danger' onclick="return confirm('Are you sre to delete?');"> Destroy</a>
            @endif</td>
          </tr>
         @endforeach
   </tbody>
</table>  
    @else
        <p> No Headphone Found </p>
    @endif
</div>
@endsection