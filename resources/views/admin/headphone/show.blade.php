@extends('inc.admin')

@section('content')
<div class="container">
<a href="{{ route('headphone.index') }}" class='btn btn-primary'>Back</a>
<h1>{{$headphone->model}}</h1>
<ul>
<li>Brand: {{$headphone->company->name}}</li>
<li>Driver: {{$headphone->driver}}</li>
<li>Frequency Response: {{$headphone->lowest_frequency}}-{{$headphone->highest_frequency}} Hz</li>
<li>Genre: {{$headphone->genre}}</li>
<li>Interface: {{$headphone->interface}}</li>
</ul>

</div>
@endsection