@extends('inc.admin')

@section('content')
<div class="container">
<a href="{{ route('company.index') }}" class='btn btn-primary'>Back</a>
<h1>New Company</h1>
{!! Form::open(['action' => 'CompanyController@store', 'method' => 'POST']) !!}
        <div class="form-group">
            {{Form::label('Name', 'Name')}}
            {{Form::text('name','',['class' => 'form-control', 'placeholder' => 'name'])}}
        </div>
        <div class="form-group">
            {{Form::label('Origin', 'Origin')}}
            {{Form::text('origin','',['class' => 'form-control', 'placeholder' => 'origin'])}}
        </div>
        {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}

</div>
@endsection