<?php
$user = Auth::user();
?>
@extends('inc.admin')

@section('content')
<div class = "container">

<h1>Company List</h1>
@if($user->isA('admin'))
<a href="{{ route('company.create') }}" class='btn btn-primary'>New Company</a>
@endif
@if(count($companies) > 0)
<table class="table">
    <thead>
        <tr>
            <th> ID</th>
            <th> Name</th>
            <th> Origin  </th>
            @if($user->isA('admin'))
            <th> Activities </th>
            @endif
        </tr>
    </thead>
    <tbody>
         @foreach($companies as $i => $company)
          <tr>
            <td> {{$i+1}} </td>
            <td> {{$company->name}} </a></td>
            <td> {{$company->origin}} </td>
            <td>@if($user->isA('admin'))
            <a href="{{ route('company.edit',['id'=>$company->id]) }}" class='btn btn-success'> Edit</a>
            <a href="{{ route('company.delete',['id'=>$company->id]) }}" class='btn btn-danger' onclick="return confirm('Are you sre to delete?');"> Destroy</a>
            @endif</td>
          </tr>
         @endforeach
   </tbody>
</table>  
    @else
        <p> No Company Found </p>
    @endif
</div>
@endsection