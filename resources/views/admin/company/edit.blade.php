@extends('inc.admin')

@section('content')
<div class="container">
<a href="{{ route('company.index') }}" class='btn btn-primary'>Back</a>
<h1>Edit {{$company->name}}</h1>
{!! Form::open(['route' => ['company.update', $company->id]
, 'method' => 'PUT']) !!}
        <div class="form-group">
            {{Form::label('Name', 'Name')}}
            {{Form::text('name', $company->name,['class' => 'form-control', 'placeholder' => 'name'])}}
        </div>
        <div class="form-group">
            {{Form::label('Origin', 'Origin')}}
            {{Form::text('origin',$company->origin,['class' => 'form-control', 'placeholder' => 'origin'])}}
        </div>
        {{Form::submit('Update', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}

</div>
@endsection