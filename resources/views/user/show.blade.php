@extends('inc.user')

@section('content')
<div class='container'>
<h1>Headphone List</h1>
@if(count($headphones) > 0)
<table class="table">
    <thead>
        <tr>
            <th> ID</th>
            <th> Model</th>
            <th> Company  </th>
            <th> Activities </th>
        </tr>
    </thead>
    <tbody>
         @foreach($headphones as $i => $headphone)
          <tr>
            <td> {{$i+1}} </td>
            <td> {{$headphone->model}} </a></td>
            <td> {{$headphone->company->name}} </td>
            </tr>
         @endforeach
   </tbody>
</table>  
    @else
        <p> No Headphone Found </p>
    @endif
@endsection