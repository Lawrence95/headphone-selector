<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeadphoneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('headphones', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('model');
            $table->unsignedInteger('company_id');
            $table->foreign('company_id')->references('id')->on('companies');
            $table->string('driver');
            $table->integer('lowest_frequency');
            $table->integer('highest_frequency');
            $table->string('genre');
            $table->string('interface');
            $table->timestamps();
        });

        DB::table('headphones')->insert([
            ['model' => 'Kraken Pro v2', 'company_id' => '3', 'driver' => '50mm Neodynium Magnets', 'lowest_frequency' => '12', 'highest_frequency' => '28000', 'genre' => 'Gaming', 'interface' => 'Wired'],
            ['model' => 'Crossfade m-100', 'company_id' => '6', 'driver' => '50mm Dual Diaphragm', 'lowest_frequency' => '5', 'highest_frequency' => '30000', 'genre' => 'Gaming', 'interface' => 'wired'],
            ['model' => 'HD 598', 'company_id' => '1', 'driver' => '50mm Neodynium Magnets', 'lowest_frequency' => '12', 'highest_frequency' => '38500', 'genre' => 'Music', 'interface' => 'Wired'],
            ['model' => 'MDR-1000X', 'company_id' => '2', 'driver' => '50mm Neodynium Magnets', 'lowest_frequency' => '4', 'highest_frequency' => '40000', 'genre' => 'Music', 'interface' => 'Wireless'],
            ['model' => 'ATH-M50x', 'company_id' => '4', 'driver' => '45mm Neodynium Magnets', 'lowest_frequency' => '15', 'highest_frequency' => '28000', 'genre' => 'Movie', 'interface' => 'Wired'],
            ['model' => 'QuietComfort 35', 'company_id' => '5', 'driver' => '50 Neodynium Magnets', 'lowest_frequency' => '10', 'highest_frequency' => '35000', 'genre' => 'Movie', 'interface' => 'Wireless'],
            ['model' => 'dt 770 pro', 'company_id' => '7', 'driver' => '45mm Neodynium Magnets', 'lowest_frequency' => '5', 'highest_frequency' => '35000', 'genre' => 'Music', 'interface' => 'Wired'],
            ['model' => 'SRH840', 'company_id' => '8', 'driver' => '40mm Neodynium Dynamic', 'lowest_frequency' => '5', 'highest_frequency' => '25000', 'genre' => 'Music', 'interface' => 'Wired'],
            ['model' => 'ManO War 7.1', 'company_id' => '3', 'driver' => '50mm Neodynium Magnets', 'lowest_frequency' => '20', 'highest_frequency' => '28000', 'genre' => 'Gaming', 'interface' => 'Wireless']

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('headphones');
    }
}
