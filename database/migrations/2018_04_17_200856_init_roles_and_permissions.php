<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\user;

class InitRolesAndPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Define Roles
        $admin = Bouncer::role()->create([
            'name' => 'admin',
            'title' => 'Administrator',
            ]);
            $member = Bouncer::role()->create([
            'name' => 'member',
            'title' => 'member',
            ]);

        //Define abilities
        $viewHeadphone = Bouncer::ability()->create([
            'name' => 'view-headphone',
            'title' => 'View headphone',
            ]);
            
        $createHeadphone = Bouncer::ability()->create([
            'name' => 'create-headphone',
            'title' => 'Create headphone',
            ]);

        $manageHeadphone = Bouncer::ability()->create([
            'name' => 'manage-headphone',
            'title' => 'Manage headphone',
            ]);

        $viewCompany = Bouncer::ability()->create([
            'name' => 'view-company',
            'title' => 'View company',
            ]);
                
        $createCompany = Bouncer::ability()->create([
            'name' => 'create-company',
            'title' => 'Create company',
            ]);
    
        $manageCompany = Bouncer::ability()->create([
            'name' => 'manage-company',
            'title' => 'Manage company',
            ]);
            
        Bouncer::allow($member)->to($viewHeadphone);
        Bouncer::allow($member)->to($viewCompany);

        Bouncer::allow($admin)->to($viewHeadphone);
        Bouncer::allow($admin)->to($createHeadphone);
        Bouncer::allow($admin)->to($manageHeadphone);
        Bouncer::allow($admin)->to($viewCompany);
        Bouncer::allow($admin)->to($createCompany);
        Bouncer::allow($admin)->to($manageCompany);

        $user = User::find(1);
        Bouncer::assign('admin')->to($user);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
