<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('origin');
            $table->timestamps();
        });

        DB::table('companies')->insert([
            ['name' => 'Senheiser', 'origin' => 'Germany', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\carbon::now()],
            ['name' => 'Sony', 'origin' => 'Japan', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\carbon::now()],
            ['name' => 'Razer', 'origin' => 'Singapore', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\carbon::now()],
            ['name' => 'Audio Technica', 'origin' => 'Japan', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\carbon::now()],
            ['name' => 'Bose', 'origin' => ' Massachusetts US', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\carbon::now()],
            ['name' => 'V-moda', 'origin' => 'Calfornia US', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\carbon::now()],
            ['name' => 'BeyerDynamic', 'origin' => 'Germany', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\carbon::now()],
            ['name' => 'Shure', 'origin' => 'Illinois US', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\carbon::now()]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
