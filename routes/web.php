<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return view('user.welcome');
});

Auth::routes();

Route::get('/admin', 'AdminController@index')->name('admin');
//Route::get('/admin/company', 'CompanyController@index');
//Route::get('/admin/company/create', 'CompanyController@create');
//Route::get('/admin/headphone', 'HeadphoneController@index');
//Route::get('/admin/headphone/create', 'HeadphoneController@create');

Route::resource('/admin/headphone', 'HeadphoneController');
Route::get('/admin/headphone/{headphone}/delete', 'HeadphoneController@destroy')->name('headphone.delete');
Route::resource('/admin/company', 'CompanyController');
Route::get('/admin/company/{company}/delete', 'CompanyController@destroy')->name('company.delete');