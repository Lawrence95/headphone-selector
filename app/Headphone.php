<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Headphone extends Model
{
    //
    protected $fillable = [
        'model',
        'company_id',
        'driver',
        'lowest_frequency',
        'highest_frequency',
        'genre',
        'interface'
    ];

    public function Company() {
        return $this->belongsTo(Company::class);
    }
}
