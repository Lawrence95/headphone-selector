<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //
    protected $fillable = [
        'name',
        'origin'
    ];

    public function Headphones() {
        return $this->hasMany(Headphone::class);
    }
}
