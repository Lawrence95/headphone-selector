<?php

namespace App\Http\Controllers;

use App\Headphone;
use App\Company;
use Illuminate\Http\Request;

class HeadphoneController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    } 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $this->authorize('view', Headphone::class);
        $headphones = Headphone::orderBy("model", 'asc')->get();
        return view('admin.headphone.index')->with('headphones', $headphones);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->authorize('create', Headphone::class);
        $company = Company::pluck('name', 'id')->toArray();
        return view('admin.headphone.create', compact('company'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'model' => 'required|max:50',
            'company_id' => 'required',
            'driver' => 'required|max:20',
            'lowest_frequency' => [
                'required',
                'regex:/^([0-9]{2})$/'
            ],
            'highest_frequency' => [
                'required',
                'regex:/^([0-9]{5})$/'
            ],
            'genre' => 'required',
            'interface' => 'required'
            ]);
            $headphone = new Headphone;
            $headphone->fill($request->all());
            $headphone->save();
            return redirect()->route('headphone.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Headphone  $headphone
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $headphone = Headphone::find($id);
        
        return view ('admin.headphone.show')->with('headphone', $headphone);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Headphone  $headphone
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
       
        $headphone = Headphone::find($id);
        $this->authorize('manage', $headphone);
        $company = Company::pluck('name', 'id')->toArray();
        return view ('admin.headphone.edit', ['headphone'=>$headphone], compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Headphone  $headphone
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $headphone = Headphone::find($id);
        if(!$headphone) throw new ModelNotFoundException;
        
        $headphone->fill($request->all());
        $headphone->save();
        return redirect()->route('headphone.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Headphone  $headphone
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $headphone = Headphone::find($id);

        $this->authorize('manage', $headphone);
        $headphone->delete();
        return redirect()->route('headphone.index');
    }

    public function list()
    {
        return view ();
    }
}
