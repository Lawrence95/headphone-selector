<?php

namespace App\Policies;

use App\User;
use App\Headphone;
use Illuminate\Auth\Access\HandlesAuthorization;

class HeadphonePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function view(User $user)
    {
        return $user->can('view-headphone');
    }

    public function create(User $user)
    {
        return $user->can('create-headphone');
    }

    public function manage(User $user ,Headphone $headphone)
    {
        return $user->can('manage-headphone');
    }
}
