<?php

namespace App\Policies;

use App\User;
use App\Company;
use Illuminate\Auth\Access\HandlesAuthorization;

class CompanyPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function view(User $user)
    {
        return $user->can('view-company');
    }

    public function create(User $user)
    {
        return $user->can('create-company');
    }

    public function manage(User $user, Company $company)
    {
        return $user->can('manage-company');
    }
}
